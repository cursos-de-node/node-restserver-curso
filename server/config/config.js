//
//Puerto
process.env.PORT = process.env.PORT || 3000;

//Entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

//Base de Datos
let urlDB;


//Vencimiento Token
//30 dias
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30;

//SEED de autenticacion
process.env.SEED = process.env.SEED || 'secret';

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/db_cafe';
} else {
    //urlDB = "mongodb+srv://mike:lVxr5f4Ik0idENOX@cluster0-yi5g5.mongodb.net/db_cafe?retryWrites=true&w=majority";
    urlDB = process.env.MONGO_URI
}

process.env.URLDB = urlDB;

process.env.CLIENT_ID = process.env.CLIENT_ID || '70823859600-tjblknqbi5u2b4kv0s89npid6fd2jak1.apps.googleusercontent.com'